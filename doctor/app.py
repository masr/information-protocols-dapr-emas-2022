import json
import logging
import sys
import time

from cloudevents.sdk.event import v1
from dapr.clients import DaprClient
from dapr.ext.grpc import App

app = App()

PUBSUB_NAME = "message_pub_sub"

def generate_rx(symptoms):
    Rx = []
    for symptom in symptoms:
        if symptom == "fever":
            Rx.append("Tylenol")
        elif symptom == "runny nose":
            Rx.append("Benadryl")
    return Rx

@app.subscribe(PUBSUB_NAME, topic='complaint')
def receive_complaint(event: v1.Event) -> None:
    # Parse incoming message
    data = json.loads(event.Data())
    print(f"Doctor <- complaint {data}", flush=True)
    # Process message based on data
    Rx = generate_rx(data)
    message = {
        "vID": data["vID"],
        "symptoms": data["symptoms"],
        "Rx": Rx
    }
    # Send message if necessary
    with DaprClient() as d:
        d.publish_event(PUBSUB_NAME, topic_name='prescription', data=json.dumps(message))

if __name__ == "__main__":
    port = sys.argv[1]
    try:
        app.run(int(port))
    except Exception as e:
        logging.error(e)
    finally:
        app.stop()