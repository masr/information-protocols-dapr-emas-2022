import json
import logging
import sys
import time
import uuid

from cloudevents.sdk.event import v1
from dapr.clients import DaprClient
from dapr.ext.grpc import App

app = App()

PUBSUB_NAME = "message_pub_sub"

@app.subscribe(PUBSUB_NAME, topic='patient')
def reception(event: v1.Event) -> None:
    data = json.loads(event.Data())
    print(f"Patient received {data}", flush=True)

@app.subscribe(PUBSUB_NAME, "filled")
def receive_filled(event: v1.Event) -> None:
    data = json.loads(event.Data())
    print(f"Patient <- filled {data}", flush=True)

if __name__ == "__main__":
    port = sys.argv[1]
    try:
        app.run(int(port))
    except Exception as e:
        logging.error(e)
    finally:
        app.stop()