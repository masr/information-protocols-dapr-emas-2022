import json
import logging
import sys

from cloudevents.sdk.event import v1
from dapr.clients import DaprClient
from dapr.ext.grpc import App

app = App()

PUBSUB_NAME = "message_pub_sub"

def generate_package(Rx):
    return [f"{rx}Bottle" for rx in Rx]

@app.subscribe(PUBSUB_NAME, topic="prescription")
def receive_prescription(event: v1.Event) -> None:

    # Parse message
    data = json.loads(event.Data())
    print(f"Pharmacist <- prescription {data}", flush=True)

    # Process message
    package = generate_package(data["Rx"])
    message = {
        "vID": data["vID"],
        "Rx": data["Rx"],
        "package": package
    }

    # Send message if necessary
    with DaprClient() as d:
        d.publish_event(PUBSUB_NAME, "filled", json.dumps(message))
        print(f"Pharmacist -> filled {message}", flush=True)

if __name__ == "__main__":
    port = sys.argv[1]
    try:
        app.run(int(port))
    except Exception as e:
        logging.error(e)
    finally:
        app.stop()

#TODO: Try out partial CoAP for pub sub