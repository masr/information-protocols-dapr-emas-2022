# Using Information Protocols in Dapr Applications

This repository is for the [EMAS 2022](https://emas.in.tu-clausthal.de/2022/) demo paper submission titled "Using Information Protocols in Dapr Applications".

## Instructions to run the applications
1. Install [dapr](https://docs.dapr.io/getting-started/install-dapr-cli/).
1. Initialize dapr components using ```dapr init```.
1. Pull docker image for [RabbitMQ](https://hub.docker.com/_/rabbitmq).
1. Start RabbitMQ docker container.
1. Install python modules using ```pip install -r requirements.txt```.
1. Start all applications by running provided `run` executable in every agent.
1. Once all applications are up and running, emulate a message being sent from the `Patient` by the command ```dapr publish --publish-app-id patient --pubsub order_pub_sub --topic complaint --data '{"vID": 1, "symptoms": ["fever", "cold"]}'```.

